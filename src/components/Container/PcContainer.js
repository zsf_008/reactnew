import React, { Component } from 'react';
import { Row, Col, Carousel, Button } from 'antd';

import ProductList from './../ProductList/productList.js';
import HotBlock from './../HotBlock/hotBlock.js';
import ProductImgBlock from './../ProductImgBlock/productImgBlock.js';
import Review from './../Review/review.js';
import HotSale from './../HotSale/hotSale.js';
import pic1 from './1.jpg';
import pic2 from './2.jpg';
import pic3 from './3.jpg';


import './PcContainer.css';



class Container extends Component {

  render() {
    return (
      <div className="container">
        <Row gutter={24}>
          <Col span={17}>
            <div className="container-left">
              {/* 轮播图 */}
              <Carousel className="pc-carousel" autoplay>
                <div><img className="banner-pic" src={pic2} alt="" /></div>
                <div><img className="banner-pic" src={pic3} alt="" /></div>
                <div><img className="banner-pic" src={pic1} alt="" /></div>
              </Carousel>

              {/* 热门资讯 */}
              <div>
                <div className="hot-title">热门资讯
                  <span className="hot-title-y">/ Hot Articles</span>
                  <a className="more" href="#">MORE+</a>
                </div>

                <ProductList count={8} type='top' />
              </div>
            </div>
          </Col>
          <Col span={7}>
            <div className="conainer-right">
              <div className="container-right-top">
                <div className="issue">
                  <Button icon="edit">  发布</Button>
                  <p style={{color:'#9B9B9B',marginTop:'10px'}}>今日主题：0 篇</p>
                </div>
                <div className="date">day /星期四</div>
              </div>

              <HotBlock count={8} type='top' itemWidth='50%' title="热门板块"/>

              <ProductImgBlock count={2} type='yule'  title="最新活动"/>
              <Review count={4} type="junshi" title='榜上有名'/>
              <HotSale count={8} type='guoji'  title="阅读榜单"/>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Container
