import React, {Component} from 'react';
import {Row, Col, BackTop} from 'antd';

import pcHeader from './../Header/pcHeader.js'
import Footer from './../Footer/footer.js'

import './pcDeatils.css';

export default class PcDetails extends Component{
  constructor(){
    super();
    this.state ={
      newsItem: ''
    }
  };
  componentDidMount(){
    const myFetchOptions ={
      method:'GET'
    };
    fetch("http://newsapi.gugujiankong.com/Handler.ashx?action=getnewsitem&uniquekey=" + this.props.params.uniquekey, myFetchOptions)
    .then(respons=>respons.json())
    .then(json=>{
      this.setState({newsItem:json})
    });
  };
  createMarkup() {
		return {__html: this.state.newsItem.pagecontent};
	};
  render(){
    return(
      <div>
				<PcHeader />
				<Row>
					<Col span={2}></Col>
					<Col span={14} className="container">
						<div class="articleContainer" dangerouslySetInnerHTML={this.createMarkup()}></div>
						<hr/>
						<CommonComments uniquekey={this.props.params.uniquekey}/>
					</Col>
					<Col span={6}>
						<PCNewsImageBlock count={40} type="top" width="100%" cardTitle="相关新闻" imageWidth="150px"/>
					</Col>
					<Col span={2}></Col>
				</Row>
				<Footer />
				<BackTop/>
			</div>
    )
  }
}
