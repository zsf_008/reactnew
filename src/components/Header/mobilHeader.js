import React, { Component } from 'react';
import {Icon, Modal, Tabs } from 'antd';

// 自定义组件
import PcLogin from './../Login/login.js';
import RegisterForm from './../Register/register.js';

import logo from './logo.svg';
import './mobilHeader.css'
const TabPane = Tabs.TabPane;


class MobilHeader extends Component {
  constructor(){
    super();
    this.state = {
      visible: false,
      hasLogined: false,
      action: 'login',   //默认选中login
    }
  };

  // 点击模态框内确定按钮
  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }
  //登录注册选中状态
  callback = (key) => {
		if (key === 1) {
			this.setState({action: 'login'});
		} else if (key === 2) {
			this.setState({action: 'register'});
		}
	};
  // 点击模态框内取消按钮
  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }
  //显示模态框
  showModal = () =>{
    this.setState({
      visible: true,
    });
  }
  render() {
    return (
      <header id='mobilheader' className="header">
        <div className='logo'>
          <img src={logo} className="logo-img" alt="logo" />
          <span>React News</span>
        </div>
        <Icon className="login-btn" type="user-add"  onClick={this.showModal} />
        <Modal title="登录 注册" visible={this.state.visible} onCancel={this.handleCancel} onOk={this.handleOk} okText="关闭">
          <Tabs type="card" onChange={this.callback}>
            <TabPane tab="登录" key="1">
              <PcLogin />
            </TabPane>
            <TabPane tab="注册" key="2">
              <RegisterForm />
            </TabPane>
          </Tabs>
        </Modal>
      </header>
    );
  }
}

export default MobilHeader;
