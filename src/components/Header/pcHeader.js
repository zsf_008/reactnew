import React, { Component } from 'react';
import logo from './logo.svg';
import { Row, Col, Menu, Icon, Modal, Button,Tabs } from 'antd';
import RegisterForm from './../Register/register.js';
import PcLogin from './../Login/login.js';
// import{ Link, } from 'react-router';
import './pcHeader.css';
const TabPane = Tabs.TabPane;


class PcHeader extends Component {
  constructor(){
    super();
    this.state = {
      current: 'home',
      visible: false,
      hasLogined: false,
      action: 'login',   //默认选中login
    }
  };

  // 点击模态框内确定按钮
  handleOk = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }
  //登录注册选中状态
  callback = (key) => {
		if (key === 1) {
			this.setState({action: 'login'});
		} else if (key === 2) {
			this.setState({action: 'register'});
		}
	};
  // 点击模态框内取消按钮
  handleCancel = (e) => {
    console.log(e);
    this.setState({
      visible: false,
    });
  }
  //显示模态框
  showModal = () =>{
    this.setState({
      visible: true,
    });
  }
  // 退出登录
  loginOut(){
    this.setState({
      hasLogined: false,
    })
  }
  render() {
		const userShow = this.state.hasLogined
			? <div key="logout" className="register">
					<a href='#'>个人中心</a>
					&nbsp;&nbsp;
					<Button ghost htmlType="button" onClick={this.loginOut.bind(this)} >退出</Button>
				</div>
			: <div key="register" className="register" >
          <Button ghost htmlType="button" onClick={this.showModal} >注册/登录</Button>
  			</div>;
    return (
      <header className="header">
        <Row className="content">
          <Col className='logo' span={6}>
            <img src={logo} className="logo-img" alt="logo" />
            <span>React News</span>
          </Col>
          <Col className="menu" span={14}>
            <Menu selectedKeys={[this.state.current]} mode="horizontal">
              <Menu.Item key="home">
                <Icon type="home" />首页
              </Menu.Item>
              <Menu.Item key="recommend"> 推荐 </Menu.Item>
              <Menu.Item key="video">  视频 </Menu.Item>
              <Menu.Item key="pic"> 图片 </Menu.Item>
              <Menu.Item key="city">  同城 </Menu.Item>
              <Menu.Item key="essencs"> 精华 </Menu.Item>
            </Menu>
          </Col>
          <Col className="login-register" span={4}>
            {userShow}
            <Modal title="登录 注册" visible={this.state.visible} onCancel={this.handleCancel} onOk={this.handleOk} okText="关闭">
              <Tabs type="card" onChange={this.callback}>
                <TabPane tab="登录" key="1">
                  <PcLogin />
                </TabPane>
                <TabPane tab="注册" key="2">
                  <RegisterForm />
                </TabPane>
              </Tabs>
            </Modal>
          </Col>
        </Row>
      </header>
    );
  }
}
export default PcHeader
