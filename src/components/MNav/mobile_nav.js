import React, {Component} from 'react';
import {Tabs} from 'antd';

const TabPane = Tabs.TabPane;

export default class Nav extends Component{
  render(){
    return(
      <Tabs>
          <TabPane tab="关注" key="1">Content of tab 1</TabPane>
          <TabPane tab="推荐" key="2">Content of tab 2</TabPane>
          <TabPane tab="视频" key="3">Content of tab 3</TabPane>
          <TabPane tab="直播" key="4">Content of tab 4</TabPane>
          <TabPane tab="图片" key="5">Content of tab 5</TabPane>
          <TabPane tab="段子" key="6">Content of tab 6</TabPane>
          <TabPane tab="精华" key="7">Content of tab 7</TabPane>
          <TabPane tab="同城" key="8">Content of tab 8</TabPane>
          <TabPane tab="游戏" key="9">Content of tab 9</TabPane>
        </Tabs>
    )
  }
}
