import React from 'react';
// import {Router, Route, Link, browserHistory} from 'react-router';

import './productImgBlock.css';
export default class ProductImgBlock extends React.Component {
	constructor() {
		super();
		this.state = {
			news: ''
		};
	}
	componentWillMount() {
		var myFetchOptions = {
			method: 'GET'
		};
		fetch("http://newsapi.gugujiankong.com/Handler.ashx?action=getnews&type=" + this.props.type + "&count=" + this.props.count, myFetchOptions)
    .then(response => response.json())
    .then(json => this.setState({news: json}));
	};
	render() {
		const {news} = this.state;
		const newsList = news.length
			? news.map((newsItem, index) => (
				<li key={index} className="product-block-item" >
					{/*<Link to={`details/${newsItem.uniquekey}`} target="_blank">*/}
						<div className="product-pic">
							<p className="product-block-date">{newsItem.date}</p>
							<img src={newsItem.thumbnail_pic_s} alt="" />
						</div>
						<p className="product-block-item-title"><span>板块名称 / </span>{newsItem.title}</p>
					{/*</Link>*/}
				</li>
			))
			: '没有加载到任何数据';
		return (
			<div className="product-img-block">
				<h2 className="product-block-title">{this.props.title}</h2>
				<ul className="product-block-list">
          {newsList}
        </ul>
			</div>
		);
	};
}
