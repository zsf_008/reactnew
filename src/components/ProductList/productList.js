import React from 'react';
import {Link} from 'react-router-dom';
import './productList.css';

export default class ProductList extends React.Component {
	constructor() {
		super();
		this.state = {
			news: ''
		};
	}
	componentWillMount() {
		var myFetchOptions = {
			method: 'GET'
		};
		fetch("http://newsapi.gugujiankong.com/Handler.ashx?action=getnews&type=" + this.props.type + "&count=" + this.props.count, myFetchOptions)
    .then(response => response.json())
    .then(json => this.setState({news: json}));
	};
	render() {
		const {news} = this.state;
		const newsList = news.length
			? news.map((newsItem, index) => (
				<li key={index}>
					<Link className="product-item" to={`details/${newsItem.uniquekey}`} target="_blank">
	          <img className="product-img" src={newsItem.thumbnail_pic_s} alt="" />
	          <div className="product-describe">
	            <h2 className="product-title">{newsItem.title}</h2>
	            <p className="product-top"><span>时间：{newsItem.date}</span> <span>作者： {newsItem.author_name}</span></p>
	            <p className="product-desc">描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述描述...</p>
	            <p className="product-bottom"><span>查看：188</span> <span>评论：20</span></p>
	          </div>
					</Link>
				</li>
			))
			: '没有加载到任何数据';
		return (
      <div className="prodct-list">
        <ul>
          {newsList}
        </ul>
        <div className="see-more-box">
          <a className="see-more" href="#" >更多资讯</a>
        </div>
      </div>

		);
	};
}
