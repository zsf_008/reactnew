import React from 'react';
import ReactDOM from 'react-dom';
import MediaQuery from 'react-responsive';
import {HashRouter, Route} from 'react-router-dom';
import Home from './routes/Home/Home.js';
import PcDetails from './routes/Details/pcDetails.js';
import MobilHome from './routes/Home/MobilHome.js';

import './index.css';
ReactDOM.render(
  <div>
    <MediaQuery query='(min-device-width: 1224px)'>
      <HashRouter>
        <div>
          <Route exact path="/" component={Home}/>
          <Route path="/details/:uniquekey" component={PcDetails} />
        </div>
      </HashRouter>
    </MediaQuery>
    <MediaQuery query='(max-device-width: 1224px)'>
      <MobilHome />
    </MediaQuery>
  </div>,
  document.getElementById('root')
);
