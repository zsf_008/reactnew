import React, {Component} from 'react';
import {BackTop} from 'antd';

import PcHeader from './../../components/Header/pcHeader.js';
import Article from './../../components/Article/article.js';
import Footer from './../../components/Footer/footer.js';

import './pcDetails.css';

export default class PcDetails extends Component{
  render(){
    return(
      <div>
        <PcHeader />
        <Article />
        <Footer />
        <BackTop />
			</div>
    )
  }
}
