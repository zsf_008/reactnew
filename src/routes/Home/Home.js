// pc端首页
import React,{Component} from 'react';
import PcHeader from './../../components/Header/pcHeader.js';
import Container from './../../components/Container/PcContainer.js';
import Footer from './../../components/Footer/footer.js';
export default class Home extends Component{
  render(){
    return(
      <div>
        <PcHeader />
        <Container />
        <Footer />
      </div>
    )
  }
}
