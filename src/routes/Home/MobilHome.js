import React,{Component} from 'react';
import MobilHeader from './../../components/Header/mobilHeader.js';
import Nav from './../../components/MNav/mobile_nav.js';
import Footer from './../../components/Footer/footer.js';
export default class MobilHome extends Component{
  render(){
    return(
      <div>
        <MobilHeader />
        <Nav />
        <Footer />
      </div>
    )
  }
}
